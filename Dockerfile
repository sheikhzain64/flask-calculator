# Use the official Python image as a base
FROM python:3.8-slim

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory in the container
WORKDIR /app

# Install dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copy the Flask app code into the container
COPY app.py .

RUN mkdir templates
# Copy the frontend HTML file into the templates folder
COPY templates/index.html templates/

# Expose the Flask port
EXPOSE 5000

# Command to run the Flask application
ENTRYPOINT ["python", "app.py"]
