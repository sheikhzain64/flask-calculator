from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/add', methods=['POST'])
def add():
    data = request.get_json()
    result = int(data.get('num1', 0)) + int(data.get('num2', 0))
    return {'result': result}

@app.route('/subtract', methods=['POST'])
def subtract():
    data = request.get_json()
    result = int(data.get('num1', 0)) - int(data.get('num2', 0))
    return {'result': result}

@app.route('/multiply', methods=['POST'])
def multiply():
    data = request.get_json()
    result = int(data.get('num1', 1)) * int(data.get('num2', 1))
    return {'result': result}

@app.route('/divide', methods=['POST'])
def divide():
    data = request.get_json()
    num1 = int(data.get('num1', 1))
    num2 = int(data.get('num2', 1))
    if num2 != 0:
        result = num1 / num2
    else:
        result = 'Error: Division by zero'
    return {'result': result}

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
