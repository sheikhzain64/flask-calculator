# Flask Calculator

This is a simple calculator application built with Flask.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.6+
- Flask
- Docker (optional)

### Installing

1. Clone the repository:
    ```bash
    git clone <your-gitlab-repo-url>
    ```

2. Navigate to the project directory:
    ```bash
    cd flask_calculator
    ```

3. Install the dependencies:
    ```bash
    pip install -r requirements.txt
    ```

## Running the Application

1. Activate the virtual environment:
    ```bash
    source venv/bin/activate
    ```

2. Run the application:
    ```bash
    python app.py
    ```

The application will be available at `http://localhost:5000`.

## Running the Tests

To run the tests, use the following command:

```bash
python -m unittest test_app.py