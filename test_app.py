import unittest
import app

class TestCalculatorMicroservice(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()

    def test_add(self):
        response = self.app.post('/add', json={'num1': 5, 'num2': 7})
        self.assertEqual(response.get_json()['result'], 12)

    def test_subtract(self):
        response = self.app.post('/subtract', json={'num1': 10, 'num2': 5})
        self.assertEqual(response.get_json()['result'], 5)

    def test_multiply(self):
        response = self.app.post('/multiply', json={'num1': 3, 'num2': 7})
        self.assertEqual(response.get_json()['result'], 21)

    def test_divide(self):
        response = self.app.post('/divide', json={'num1': 10, 'num2': 2})
        self.assertEqual(response.get_json()['result'], 5)

    def test_divide_by_zero(self):
        response = self.app.post('/divide', json={'num1': 10, 'num2': 0})
        self.assertEqual(response.get_json()['result'], 'Error: Division by zero')

if __name__ == '__main__':
    unittest.main()